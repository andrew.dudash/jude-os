#include "terminal.h"
#include "memory.h"

ssize_t terminal_write(const char * data, size_t size, struct raw_output_stream * stream) {
  return stream->write(data, size);
}

ssize_t terminal_write_string(const char * data, struct raw_output_stream * stream) {
  return terminal_write(data, strlen(data), stream);
}
