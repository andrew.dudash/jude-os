#ifndef TERMINAL_H
#define TERMINAL_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef size_t ssize_t;

struct raw_output_stream {
  ssize_t (*write)(const char *, size_t size);
};

ssize_t device_write(const char *, size_t size, struct raw_output_stream * stream);
ssize_t terminal_write_string(const char * data, struct raw_output_stream * stream);

#endif
