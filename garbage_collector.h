#ifndef GARBAGE_COLLECTOR_H
#define GARBAGE_COLLECTOR_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

extern const struct memory_object memory_null_object;

enum memory_object_type {
  MEMORY_FREE,
  MEMORY_FIXED_INTEGER,
  MEMORY_PAIR,
  MEMORY_NULL,
};

enum memory_mark {
  MEMORY_MARK,
  MEMORY_CLEAR
};

struct memory_object {
  enum memory_object_type type;
  enum memory_mark mark;
  union {
    size_t fixed_integer;
    struct {struct memory_object * car; struct memory_object * cdr;} pair;
  };
};

struct memory_pool {
  struct memory_object * start;
  size_t size;
};

struct memory_root {
  struct memory_object ** start;
  size_t size;
};

void memory_initialize(const struct memory_pool raw, const struct memory_root root);
struct memory_object * memory_allocate_object();

struct memory_object * memory_new_object();
void memory_free_object(struct memory_object * object);

struct memory_object * memory_new_fixed_integer(int value);
struct memory_object * memory_new_cons(struct memory_object * car, struct memory_object * cdr);
void memory_set_cons_car(struct memory_object * target, struct memory_object * car);
void memory_set_cons_cdr(struct memory_object * target, struct memory_object * cdr);

struct memory_object ** memory_add_root(struct memory_object * object);
void memory_remove_root(struct memory_object ** object);
void memory_mark_recurse(struct memory_object * object);
void memory_mark_world();
void memory_sweep_world();
void memory_run_garbage_collector();

void memory_mark_root(struct memory_object ** object);
int memory_root_is_marked(struct memory_object ** object);

#endif
