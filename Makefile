CC=~/opt/cross/bin/i686-elf-gcc
CFLAGS=-std=gnu99 -ffreestanding -O2 -Wall -Wextra
AS=~/opt/cross/bin/i686-elf-as
OBJECTS = kernel.o terminal.o memory.o garbage_collector.o vga.o

jude-os.iso: jude-os.bin grub.cfg
	mkdir -p isodir/boot/grub
	cp jude-os.bin isodir/boot/
	cp grub.cfg isodir/boot/grub/
	grub-mkrescue -o jude-os.iso isodir

jude-os.bin: linker.ld boot.o $(OBJECTS)
	$(CC) -T linker.ld -o jude-os.bin -ffreestanding -O2 -nostdlib boot.o garbage_collector.o kernel.o terminal.o vga.o memory.o -lgcc

$(objects) : %.o : %.c
	$(CC) -c $(CFLAGS) $< $@

boot.o: boot.s
	$(AS) boot.s -o boot.o
