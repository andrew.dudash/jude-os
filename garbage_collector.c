#include "garbage_collector.h"

const struct memory_object memory_null_object = {
  .type=MEMORY_NULL
};

struct memory_pool memory_raw;
struct memory_root memory_root;

static inline void memory_mark_object(struct memory_object * object) {
  object->mark = MEMORY_MARK;
}

static inline void memory_clear_object(struct memory_object * object) {
  object->mark = MEMORY_CLEAR;
}

void memory_free_object(struct memory_object * object) {
  object->type = MEMORY_FREE;
}

static inline void memory_clear_root(struct memory_object ** object) {
  *object = (struct memory_object *)((uintptr_t)*object | 1);
}

void memory_initialize(const struct memory_pool raw, const struct memory_root root) {
  for (size_t index = 0; index < raw.size; index++) {
    raw.start[index] = (struct memory_object) {0};
    memory_free_object(raw.start + index);
    memory_clear_object(raw.start + index);
  }
  memory_raw = raw;

  for (size_t index = 0; index < root.size; index++) {
    root.start[index] = NULL;
    memory_clear_root(root.start + index);
  }
  memory_root = root;
}

void memory_mark_root(struct memory_object ** object) {
  *object = (struct memory_object *)((uintptr_t)*object & ~1);
}

int memory_root_is_marked(struct memory_object ** object) {
  return !((uintptr_t)*object & 1);
}

struct memory_object ** memory_add_root(struct memory_object * object) {
  for (size_t index = 0; index < memory_root.size; index++) {
    if (!memory_root_is_marked(memory_root.start + index)) {
      memory_root.start[index] = object;
      memory_mark_root(memory_root.start + index);
      return memory_root.start + index;
    }
  }
  return NULL;
}

void memory_remove_root(struct memory_object ** object) {
  memory_clear_root(object);
}

static inline int memory_object_is_marked(struct memory_object * object) {
  return object->mark == MEMORY_MARK;
}

void memory_mark_recurse(struct memory_object * object) {
  if (memory_object_is_marked(object)) {
    return;
  }
  switch (object->type) {
  case MEMORY_FIXED_INTEGER:
    memory_mark_object(object);
    break;
  case MEMORY_PAIR:
    memory_mark_object(object);
    memory_mark_recurse(object->pair.car);
    memory_mark_recurse(object->pair.cdr);
    break;
  case MEMORY_NULL:
    break;
  case MEMORY_FREE:
    break; //TODO!
  default:
    break; //TODO!
  }
}

int memory_object_is_free(struct memory_object * object) {
  return object->type == MEMORY_FREE;
}

struct memory_object * memory_allocate_object() {
  for (size_t index = 0; index < memory_raw.size; index++) {    
    if (memory_object_is_free(memory_raw.start + index)) {
      return memory_raw.start + index;
    }
  }
  return NULL;
}

struct memory_object * memory_new_object() {
  struct memory_object * object = memory_allocate_object();
  if (object != NULL) {
    return object;
  }
  memory_run_garbage_collector();
  object = memory_allocate_object();
  if (object != NULL) {
    return object;
  }
  return NULL; //TODO!
}

struct memory_object * memory_new_fixed_integer(int value) {
  struct memory_object * object = memory_new_object();
  if (object == NULL) {
    return NULL;
  }
  object->type = MEMORY_FIXED_INTEGER;
  object->fixed_integer = value;
  return object;
}

struct memory_object * memory_new_cons(struct memory_object * car, struct memory_object * cdr) {
  struct memory_object * object = memory_new_object();
  if (object == NULL) {
    return NULL;
  }
  object->type = MEMORY_PAIR;
  object->pair.car = car;
  object->pair.cdr = cdr;
  return object;
}

void memory_set_cons_car(struct memory_object * target, struct memory_object * car) {
  target->pair.car = car;
}

void memory_set_cons_cdr(struct memory_object * target, struct memory_object * cdr) {
  target->pair.cdr = cdr;
}

void memory_mark_world() {
  for (size_t index = 0; index < memory_root.size; index++) {
    if (memory_root_is_marked(memory_root.start + index)) {
      memory_mark_recurse(memory_root.start[index]);
    }
  }
}

void memory_sweep_world() {
  for (size_t index = 0; index < memory_raw.size; index++) {
    if (!memory_object_is_marked(memory_raw.start + index)) {
      memory_free_object(memory_raw.start + index);
    }
    memory_clear_object(memory_raw.start + index);
  }
}

void memory_run_garbage_collector() {
  memory_mark_world();
  memory_sweep_world();
}

