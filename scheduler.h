#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <stdbool.h>
#include <stddef.h>


typedef size_t process_priority;
typedef struct scheduler_process * process_identity;

enum scheduler_process_status {
  PROCESS_STATUS_NULL,
  PROCESS_STATUS_READY,
  PROCESS_STATUS_CURRENT
};

struct scheduler_process {
  enum scheduler_process_status status;
  process_identity parent_process;
  process_priority priority;
  char * stack_pointer;
  char * stack_base;
};

struct scheduler_process_pool {
  struct scheduler_process * start;
  size_t size;
};

struct scheduler_ready_pool {
  struct scheduler_process ** start;
  size_t size;
  size_t count;
};

void scheduler_initialize(struct scheduler_process_pool process_pool, struct scheduler_ready_pool ready_pool);
void scheduler_clear_process(struct scheduler_process * process);
void scheduler_null_process(process_identity process);

process_identity scheduler_add_process(struct scheduler_process * template);
void scheduler_ready_pool_add_process(struct scheduler_process * process);
struct scheduler_process * scheduler_ready_pool_peek_process();
struct scheduler_process * scheduler_ready_pool_get_process();
void scheduler_context_switch(char ** old_stack_pointer, char ** new_stack_pointer);

#endif
