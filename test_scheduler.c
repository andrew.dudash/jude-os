#include <stdio.h>

#include "scheduler.h"

#define TEST_PROCESS_POOL_SIZE 4
#define TEST_READY_POOL_SIZE 4

void print_process(struct scheduler_process * process) {
  printf("Process Information:\n");
  printf("\tIdentity: %p\n", process);
  printf("\tStatus: ");
  switch(process->status) {
  case PROCESS_STATUS_NULL:
    printf("Null");
    break;
  case PROCESS_STATUS_READY:
    printf("Ready");
    break;
  case PROCESS_STATUS_CURRENT:
    printf("Current");    
    break;
  default:
    printf("Unknown Code [%d]", process->status);
  }
  printf("\n");
  printf("\tParent Process: %p\n", process->parent_process);
  printf("\tStack Pointer: %p\n", process->stack_pointer);
  printf("\tStack Base: %p\n", process->stack_base);
}

int main() {
  struct scheduler_process process_pool_start[TEST_PROCESS_POOL_SIZE];
  size_t process_pool_size = TEST_PROCESS_POOL_SIZE;
  struct scheduler_process_pool process_pool = {
    .start = process_pool_start,
    .size = process_pool_size
  };

  struct scheduler_process * ready_pool_start[TEST_READY_POOL_SIZE];
  size_t ready_pool_size = TEST_READY_POOL_SIZE;
  struct scheduler_ready_pool ready_pool = {
    .start = ready_pool_start,
    .size = ready_pool_size,
    .count = 0
  };
  
  scheduler_initialize(process_pool, ready_pool);

  struct scheduler_process template = {
    .parent_process = 0,
    .priority = 0
  };
  process_identity new_process = scheduler_add_process(&template);
  if (new_process == NULL) {
    printf("Failed to add process.\n");
  }
  print_process(new_process);
  scheduler_ready_pool_add_process(new_process);
  process_identity process = scheduler_ready_pool_peek_process();  
  print_process(process);
  
  return 0;
}
