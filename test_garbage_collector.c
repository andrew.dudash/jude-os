#include <stdio.h>

#include "garbage_collector.h"

#define TEST_MEMORY_POOL_SIZE 4
#define TEST_MEMORY_ROOT_SIZE 4

void print_memory(struct memory_object * object) {
  switch (object->type) {
  case MEMORY_FIXED_INTEGER:
    printf("INTEGER: %d", object->fixed_integer);
    break;
  case MEMORY_PAIR:
    printf("PAIR: %p", object->pair);
    break;
  default:
    printf("UNKNOWN: %d", object->type);
  }
}

int main() {
  struct memory_object memory_pool_array[TEST_MEMORY_POOL_SIZE];
  size_t memory_pool_size = TEST_MEMORY_POOL_SIZE;
  struct memory_pool pool = {
    .start = memory_pool_array,
    .size = memory_pool_size
  };

  struct memory_object * memory_root_array[TEST_MEMORY_ROOT_SIZE];
  size_t memory_root_size = TEST_MEMORY_ROOT_SIZE;
  struct memory_root root = {
    .start = memory_root_array,
    .size = memory_root_size
  };
  
  memory_initialize(pool, root);

  struct memory_object ** test = memory_add_root(memory_new_fixed_integer(10));  
  print_memory(*test);
  printf("\n");
  
  printf("Is root active: [%d]\n", memory_root_is_marked(test));
  memory_remove_root(test);
  printf("Is root active: [%d]\n", memory_root_is_marked(test));
  printf("\n");

  test = memory_add_root(memory_new_fixed_integer(10));
  print_memory(*test);
  printf("\n");

  printf("Is root active: [%d]\n", memory_root_is_marked(test));
  memory_remove_root(test);
  printf("Is root active: [%d]\n", memory_root_is_marked(test));
  printf("\n");

  struct memory_object ** test1 = memory_add_root(memory_new_fixed_integer(10));
  struct memory_object ** test2 = memory_add_root(memory_new_fixed_integer(10));
  print_memory(*test1);
  printf("\n");

  printf("Is root active: [%d]\n", memory_root_is_marked(test1));
  memory_remove_root(test1);
  printf("Is root active: [%d]\n", memory_root_is_marked(test1));
  printf("\n");

  print_memory(*test2);
  printf("\n");
  printf("Is root active: [%d]\n", memory_root_is_marked(test2));
  memory_remove_root(test2);
  printf("Is root active: [%d]\n", memory_root_is_marked(test2));
  printf("\n");

  memory_initialize(pool, root);
  for (size_t count = 0; count < 32; count++) {
    struct memory_object * test = memory_new_fixed_integer(count);
    printf("[%03d]\t", count);
    if (test == NULL) {
      printf("Null");
    } else {
      print_memory(test);
    }
    printf("\n");
  }

  return 0;
}
