#include "terminal.h"
#include "garbage_collector.h"
#include "vga.h"

#define KERNEL_MEMORY_POOL_SIZE 8
#define KERNEL_MEMORY_ROOT_SIZE 8

void print_memory(struct memory_object * object, struct raw_output_stream * out) {
  switch (object->type) {
  case MEMORY_FIXED_INTEGER:
    terminal_write_string("INTEGER", out);
    break;
  case MEMORY_PAIR:
    terminal_write_string("PAIR", out);
    print_memory(object->pair.car, out);
    print_memory(object->pair.cdr, out);
    break;
  case MEMORY_NULL:
    terminal_write_string("NULL", out);
    break;
  default:
    terminal_write_string("UNKNOWN", out);
  }
}

void kernel_main(void) {
  vga_initialize();
  terminal_write_string("Starting GC\n", &vga_raw_output_stream);

  struct memory_object memory_pool_array[KERNEL_MEMORY_POOL_SIZE];
  size_t memory_pool_size = KERNEL_MEMORY_POOL_SIZE;
  struct memory_pool pool = {
    .start = memory_pool_array,
    .size = memory_pool_size
  };

  struct memory_object * memory_root_array[KERNEL_MEMORY_ROOT_SIZE];
  size_t memory_root_size = KERNEL_MEMORY_ROOT_SIZE;
  struct memory_root root = {
    .start = memory_root_array,
    .size = memory_root_size
  };
  
  memory_initialize(pool, root);

  for (size_t count = 0; count < 9; count++) {
    struct memory_object * test = memory_new_fixed_integer(10);
    if (test == NULL) {
      terminal_write_string("Null\n", &vga_raw_output_stream);
      continue;
    }
    print_memory(test, &vga_raw_output_stream);
    terminal_write_string("\n", &vga_raw_output_stream);
  }
  struct memory_object * test = memory_new_cons(
    memory_new_fixed_integer(10),
    memory_new_cons(
      memory_new_fixed_integer(20),
      memory_new_cons(
        memory_new_fixed_integer(30),
	&memory_null_object
  )));
  //memory_set_cons_car(test, test);
  //memory_set_cons_cdr(test, test);
  print_memory(test, &vga_raw_output_stream);
  terminal_write_string("\n", &vga_raw_output_stream);

  terminal_write_string("Started GC\n", &vga_raw_output_stream);
}
