#include "scheduler.h"
#include <stdio.h>

struct scheduler_process_pool scheduler_process_pool;
struct scheduler_ready_pool scheduler_ready_pool;
process_identity current_process;

void scheduler_initialize(struct scheduler_process_pool process_pool, struct scheduler_ready_pool ready_pool) {
  for (size_t index = 0; index < process_pool.size; index++) {
    scheduler_null_process(process_pool.start + index);
  }
  scheduler_process_pool = process_pool;

  for (size_t index = 0; index < ready_pool.size; index++) {
    ready_pool.start[index] = NULL;
  }
  scheduler_ready_pool = ready_pool;

  current_process = 0;
}

void scheduler_clear_process(struct scheduler_process * process) {
  process->status = PROCESS_STATUS_NULL;
}

void scheduler_reschedule() {
  struct scheduler_process * old_process = current_process;
  if (old_process->status == PROCESS_STATUS_CURRENT) {
    old_process->status = PROCESS_STATUS_READY;
    scheduler_ready_pool_add_process(old_process);
  }

  //current_process = scheduler_ready_pool_get_process();
  //current_process->status = PROCESS_STATUS_READY;
  //scheduler_context_switch(&old_process->stack_pointer, &current_process->stack_pointer);
}


struct scheduler_process_ready_list {
  struct scheduler_process * process;
};

process_identity scheduler_add_process(struct scheduler_process * template) {
  for (size_t index = 0; index < scheduler_process_pool.size; index++) {
    process_identity candidate = scheduler_process_pool.start + index;
    if (candidate->status == PROCESS_STATUS_NULL) {
      candidate->status = PROCESS_STATUS_READY;
      candidate->parent_process = template->parent_process;
      candidate->priority = template->priority;
      candidate->stack_pointer = NULL;
      candidate->stack_base = NULL;
      return candidate;
    }
  }
  return NULL; //TODO!
}

int scheduler_is_valid_process_identity(process_identity process) {
  return (scheduler_process_pool.start <= process) && (process <= scheduler_process_pool.start + scheduler_process_pool.size);
}

void scheduler_null_process(process_identity process) {
  process->status = PROCESS_STATUS_NULL;
  process->parent_process = 0;
  process->priority = 0;
  process->stack_pointer = NULL;
  process->stack_base = NULL;
}

void scheduler_remove_process(process_identity process) {
  if (!scheduler_is_valid_process_identity(process)) {
    return; //TODO
  }
  if (process->status == PROCESS_STATUS_NULL) {
    return; //TODO
  }
  scheduler_null_process(process);
}

void scheduler_ready_pool_add_process(struct scheduler_process * process) {
  if (scheduler_ready_pool.count == scheduler_ready_pool.size) {
    return; //TODO!
  }

  size_t hole = ++scheduler_ready_pool.count;
  for (; hole > 1 && process->priority < scheduler_ready_pool.start[hole / 2]->priority; hole /= 2) {
    scheduler_ready_pool.start[hole] = scheduler_ready_pool.start[hole / 2];
  }
  scheduler_ready_pool.start[hole] = process;
}

process_identity scheduler_ready_pool_peek_process() {
  if (scheduler_ready_pool.count == 0) {
    return NULL; //TODO!
  }
  return scheduler_ready_pool.start[1];
}

int scheduler_compare_process_priority(process_identity first, process_identity second) {
  return second->priority - first->priority;
}


struct scheduler_process * scheduler_ready_pool_get_process() {
  struct scheduler_process * ready_process = scheduler_ready_pool_peek_process();
  if (ready_process == NULL) {
    return NULL; //TODO!
  }

  size_t hole = 0;
  process_identity * pool = scheduler_ready_pool.start;
  process_identity temp = pool[hole];
  size_t child;
  for (; hole * 2 <= scheduler_ready_pool.count; hole = child) {
    size_t child = hole * 2;
    if (child != scheduler_ready_pool.count) {
      if (scheduler_compare_process_priority(pool[child], pool[child + 1]) > 0) {
	child++;
      }
    }

    if (scheduler_compare_process_priority(pool[child], temp) > 0) {
      pool[hole] = pool[child];
    } else {
      break;
    }
  }
  scheduler_ready_pool.start[hole] = temp;

  return ready_process;
}
