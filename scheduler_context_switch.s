	.text
	.globl scheduler_context_switch

	pushl %ebp
	movl %esp, %ebp
	pushfl
	pushal

	movl 8(%ebp), %eax
	movl %esp, (%eax)
	movl 12(%ebp), %eax

	movl (%eax), %esp

	popal
	movl 4(%esp), %ebp
	popfl
	add $4, %esp
	ret
